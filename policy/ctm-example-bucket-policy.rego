package terraform.analysis

# Versioning is enabled
# Logging is enabled
# Encryption has been enabled
# Tags are one of ....
# No generous policies

import input as tfplan

default isS3VersionEnabled = false
isS3VersionEnabled {
    some i, j;
    input.planned_values.root_module.resources[i].values.versioning[j].enabled == true
}

default isS3CWLogEnabled = false
isS3CWLogEnabled {
    some i, j;
    count(input.planned_values.root_module.resources[i].values.logging[j].target_bucket) > 0
}

# Three Rules below with same name are Rego way of Logical OR
default isS3EncryptionEnabled = false
isS3EncryptionEnabled {
    some i, j, k, l;
    input.planned_values.root_module.resources[i].values.server_side_encryption_configuration[j].rule[k].apply_server_side_encryption_by_default[l].sse_algorithm == "AES256"
}

isS3EncryptionEnabled {
    some i, j, k, l;
    input.planned_values.root_module.resources[i].values.server_side_encryption_configuration[j].rule[k].apply_server_side_encryption_by_default[l].sse_algorithm == "AES128"
}

isS3EncryptionEnabled {
    some i, j, k, l;
    input.planned_values.root_module.resources[i].values.server_side_encryption_configuration[j].rule[k].apply_server_side_encryption_by_default[l].sse_algorithm == "AES192"
}

default isS3AccessIsRestricted = false
isS3AccessIsRestricted {
    input.planned_values.root_module.resources[i].values.acl == "private"
}

default isS3TagsSet = false
isS3TagsSet {
    count(input.planned_values.root_module.resources[i].values.tags.name) > 0
    count(input.planned_values.root_module.resources[i].values.tags.product_team) > 0
    count(input.planned_values.root_module.resources[i].values.tags.product_name) > 0
    count(input.planned_values.root_module.resources[i].values.tags.environment) > 0
    count(input.planned_values.root_module.resources[i].values.tags.service) > 0
    count(input.planned_values.root_module.resources[i].values.tags.owner) > 0
    count(input.planned_values.root_module.resources[i].values.tags.terraform) > 0
    count(input.planned_values.root_module.resources[i].values.tags.pii) > 0
}

# Unit Testing Infrastructure as Code

Infrastructure As Code (IaC) is declarative code that creates stateless infrastructure. There are several IaC declarative languages available in the market. In any case, these declarative languages are yet another wrapper on top of the native APIs provided by the cloud providers. <BR>

Hence, there is a need to verify and validate IaC.<BR>

The IaC code used in this repository uses declarative language from [terraform](https://www.terraform.io/). The policy engine [Open policy agent](https://github.com/open-policy-agent/opa) is used.

The objective of this test is to validate the terraform plan in line with the company's infrastructure policy early in the development phase/pipeline. 

## Software pre-requisistes
- terraform, version 0.12.20 onwards.

## Software Installation
The easiest way to install `opa` is via [brew](https://brew.sh/).
```
> brew install opa
```
Verify that `opa` is successfully installed by issuing this command
```
> opa version
Version: 0.17.1
Build Commit:
Build Timestamp:
Build Hostname:
```

## Execute the test
Generate policy file in JSON format as below
```
> cd ./infra
> terraform init
> terraform plan -out s3.plan
> terraform show -json s3.plan | tee  ctm-example-bucket-plan.json

> # Remove temporary files
> rm -rf s3.plan

> # Return to the project root folder
> cd ../
```

Validate terrafrom plan against the policy.
```
> opa eval --data ./policy/ctm-example-bucket-policy.rego --input ./infra/ctm-example-bucket-plan.json "data.terraform.analysis"
```

A successful policy validation should look like this.
```
macbook:infrastructure-as-code-test$ opa eval --data ./policy/ctm-example-bucket-policy.rego --input ./infra/ctm-example-bucket-plan.json "data.terraform.analysis"
{
  "result": [
    {
      "expressions": [
        {
          "value": {
            "isS3AccessIsRestricted": true,
            "isS3CWLogEnabled": true,
            "isS3EncryptionEnabled": true,
            "isS3TagsSet": true,
            "isS3VersionEnabled": true
          },
          "text": "data.terraform.analysis",
          "location": {
            "row": 1,
            "col": 1
          }
        }
      ]
    }
  ]
}
```


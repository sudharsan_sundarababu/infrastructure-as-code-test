provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "ctm-vehicle-abi-data-prod" {
  bucket = "ctm-vehicle-abi-data-prod"
  acl    = "private"

  versioning {
    enabled = "${var.s3_bucket_versioning_enabled}"
  }

  logging {
    target_bucket = "${var.target_bucket}"
    target_prefix = "ctm-vehicle-abi-data-prod/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = {
    name         = "ctm-vehicle-abi-data-prod"
    product_team = "carinsurance"
    product_name = "abi"
    environment  = "prod"
    service      = "carinsurance.vehicle-abi-automated-processor.node"
    owner        = "carinsuranceteam@bglgroup.co.uk"
    terraform    = "true"
    pii          = "false"
  }
}

resource "aws_s3_bucket_policy" "ctm-vehicle-abi-data-prodbucket_policy" {
  bucket = "${aws_s3_bucket.ctm-vehicle-abi-data-prod.id}"

  policy = <<POLICY
{
  "Id": "Policy1537121123345",
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Stmt1537365665157",
      "Action": [
        "s3:DeleteBucket"
      ],
      "Effect": "Deny",
      "Resource": [
        "arn:aws:s3:::ctm-vehicle-abi-data-prod",
        "arn:aws:s3:::ctm-vehicle-abi-data-prod/*"
      ],
      "Principal": "*"
    },
    {
      "Sid": "Stmt1537365816157",
      "Action": [
        "s3:Get*",
        "s3:Put*",
        "s3:List*"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::ctm-vehicle-abi-data-prod",
        "arn:aws:s3:::ctm-vehicle-abi-data-prod/*"
      ],
      "Principal": {
        "AWS": [
          "arn:aws:iam::132988281561:role/continuousdelivery-agent"
        ]
      }
    }
  ]
}
POLICY
}

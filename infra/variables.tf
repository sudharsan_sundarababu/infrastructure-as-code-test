variable "s3_bucket_versioning_enabled" {
  description = "set whether versioning is enabled or not"
  type = "string"
  default = "true"
}

variable "target_bucket" {
  description = "name of the target bucket"
  type = "string"
  default = "ctm-default-logging-bucket"
}
